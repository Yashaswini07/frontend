import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import registerServiceWorker from "./registerServiceWorker";
import FileUpload from "./components/fileupload.jsx";

ReactDOM.render(<FileUpload />, document.getElementById("root"));
registerServiceWorker();
