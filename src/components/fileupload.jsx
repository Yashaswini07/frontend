import React, { Component } from 'react';
import { storage } from '../firebase';
import CKEditor from '../ckeditor/ckeditor';

class FileUpload extends Component {
    state = {
        file: null,
        url: '',
        progress: 0
    }
    // handleChange = this.handleChange.bind(this);
    handleChange = e => {
        if (e.target.files[0]) {
            const file = e.target.files[0];
            this.setState(() => ({ file }));
        }
    }
    handleUpload = () => {
        const { file } = this.state;
        const uploadTask = storage.ref(`files/${file.name}`).put(file);
        uploadTask.on('state_changed',
            (snapshot) => {
                //progress function....
                const progress = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
                this.setState({ progress });
            },
            (error) => {
                //error function
                console.log(error);
            },
            () => {
                //complete function
                storage.ref('files').child(file.name).getDownloadURL().then(url => {
                    console.log(url);
                    this.setState({ url });
                })
            });

    }
    render() {
        const style = {
            height: '100vh',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center'
        };
        return (
            <div style={style}>
                <progress value={this.state.progress} max="100" />
                <br />
                <input type="file" onChange={this.handleChange} />
                <button onClick={this.handleUpload}>Upload</button>
                <br />
                <img src={this.state.url || 'https://upload.wikimedia.org/wikipedia/commons/thumb/0/0c/File_alt_font_awesome.svg/512px-File_alt_font_awesome.svg.png'} alt="Uploaded files" height="50" width="50" />
                <object data={this.state.url} width="300" height="300">
                     <a id="display" href={this.state.url}>test.pdf</a>
                </object>
            </div>
        )
    }
}
export default FileUpload;