import firebase from 'firebase/app';
import 'firebase/storage';
// import CKEditor from "react-ckeditor-component";
// module.exports = require('./ckeditor');
// Initialize Firebase
var config = {
    apiKey: "AIzaSyBv1MHUpD31yYR4r51UG_CdCaE_K67MG8c",
    authDomain: "file-collaborator.firebaseapp.com",
    databaseURL: "https://file-collaborator.firebaseio.com",
    projectId: "file-collaborator",
    storageBucket: "file-collaborator.appspot.com",
    messagingSenderId: "305313362358"
};
firebase.initializeApp(config);
const storage = firebase.storage();
export {
    storage, firebase as default
}